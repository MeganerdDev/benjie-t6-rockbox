Rockbox guide for Benjie T6 (Benjie K1). The new AGPTEK Rocker that has issues upgrading from 1.08B firmware.

## ROCKER Firmware Update(7th, Dec, 2018) - Firmware download
https://www.agptek.com/blog/agptek-rocker-firmware-download-2/
http://images.agptek.us/Download/AGPTEK_Rocker%20firmware%20update(20181207).zip


## Linux

>>>
underst0rm
>>>
```
$ sudo apt-get update && sudo apt-get install xorriso unrar

# Download imagearchive and extract image
$ wget http://images.agptek.us/Download/AGPTEK_ROCKER\(20171101\).rar
$ unrar e AGPTEK_ROCKER\(20171101\).rar

# Extract the internal image files into the "iso" folder
xorriso -osirrox on -ecma119_map lowercase -indev ROCKER_update.upt -extract / iso

# Open iso/version.txt, Replace ver=1.0.0.0 with ver=2018-10-07T00:00:00+08:00

# Repack into an update image
xorriso -as mkisofs -volid 'CDROM' --norock -output uROCKER_update_1.8_beta.upt iso
```

## Windows

>>>
shikotee

For those who have been stuck with T6's 1.08B firmware, a Linux user in the Rockbox forums came up with a solution.

This is what I did to make it happen on Windows.

I changed the .upt extension to .iso, and extracted the contents to a folder (using trial version of PowerISO).
You need to make modifications to "Version.txt".
Replace "ver=1.0.0.0" with "ver=2018-10-07T00:00:00+08:00"
Using IMGBurn, I rebuilt the iso, and changed the extension back to .upt
I transferred update.upt onto my microsd, and was then able to update my 1.08B FW to AGPtek 1.2.
The interface seems pretty much the same - just more colourful.

Will play around with 1.2 today, and will explore installing Rockbox this week-end.

I was also able to resolve my duplicates problem using DigitalVolcano Duplicate Cleaner Pro.
It can be set specifically for music, and has a wide range of ways of scanning audio files to vet duplicates.
I was able to reduce my two main folders to around 16500 music files.
I then removed two genres (jazz and classical) so that I would be under the 15000 limit.
They now have separate folders, and I will use playlists to access those genres.

Naturally, my assumption is that I will not have such a limit once I install Rockbox.
>>>


## Downloadable upt I patched from ROCKER Firmware Update(7th, Dec, 2018).
### I tested this, upgraded on my 1.08B Benjioe T6.



1.  Download [this Firmware](https://drive.google.com/file/d/1utL0YDdwvyLcRDTZ5aOvW8aP1Jw4q4vm/view?usp=sharing) I made using the above steps in Debian
2.  Rename file to update.upt
3.  Copy to root of SD card
4.  Download [rockbox.zip](https://drive.google.com/file/d/1LI1Xnd-JzHFih3JIkVV3RRnhg62dY5zX/view)
5.  Extract .rockbox folder to root of SD card
6.  Boot OS -> Firmware Update
7.  You will see `V V Succeed` on the screen and `Upgrading` (if it doesn't work try again)
8.  Boot in to the normal OS at the bootloader on first boot
9.  Reboot, you can now boot in Rockbox

You will now be dual booting Rockbox and Official firmware 1.2

## Sources
```
https://forums.rockbox.org/index.php/topic,51653.240.html
https://www.head-fi.org/threads/the-shenzhen-benjie-bj-t6-agptek-rocker.834797/page-44
https://www.head-fi.org/threads/the-shenzhen-benjie-bj-t6-agptek-rocker.834797/
```

## Rockbox themes
Any themes that are 128x160 will work (Eg [GoGearSA9200 themes](http://themes.rockbox.org/index.php?target=gogearsa9200))

If you modify a bmp background then export as (128x160x16) BMP (Note: 16-bit color depth!)
[See 13.1.6. Loading Backdrops (Page 186) Rockbox user manual](https://download.rockbox.org/manual/rockbox-iriverh300.pdf)

